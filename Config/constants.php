<?php

define('DS', DIRECTORY_SEPARATOR);
define('SYSTEM_PATH', str_replace(DS . 'Config', '', __DIR__));
define('CONFIG', __DIR__);
define('APP_PATH', SYSTEM_PATH . DS . 'Application');
define('CONTROLLER', APP_PATH . DS . 'Controller');
define('FILE_SOURCE', APP_PATH . DS . 'FileStorage' . DS . 'FileSource');
define('MODEL', APP_PATH . DS . 'Model');
define('VIEW', APP_PATH . DS . 'View');
define('URI', str_replace('index.php', '', $_SERVER['DOCUMENT_URI']));