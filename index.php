<?php

require __DIR__ . '/vendor/autoload.php';
require_once 'Library' . DIRECTORY_SEPARATOR . 'Autoloader.php';
require __DIR__ . DIRECTORY_SEPARATOR . 'Config' . DIRECTORY_SEPARATOR . 'constants.php';
require __DIR__ . DIRECTORY_SEPARATOR . 'Library' . DIRECTORY_SEPARATOR . 'functions.php';

\Library\Router::run();