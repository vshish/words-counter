<?php

class DictionaryController extends \Application\Controller\AController
{

	/**
	 * @var string - name Controller
	 */
	public $name = 'Dictionary';

	/**
	 * Action for show all words in Dictionary
	 */
	public function index()
	{
		$Dictionary = $this->_loadModel('Dictionary');
		$words = $Dictionary->getWords();

		$this->_set('words', $words);
		$this->_render();
	}

	/**
	 * Action for show all words in Dictionary
	 */
	public function userWords()
	{
		$userId = 1;

		$UserDictionary = $this->_loadModel('UserDictionary');
		$words = $UserDictionary->getWords($userId);

		$this->_set('userId', $userId);
		$this->_set('words', $words);
		$this->_render('user_words');
	}

	/**
	 * Write word in db
	 */
	public function write() {
		var_dump($_GET['word']);
		if (empty($_GET['word']) || empty($_GET['userId'])) {
			return;
		}

		$UserDictionary = $this->_loadModel('UserDictionary');
		$UserDictionary->setUserId($_GET['userId']);

		if ($UserDictionary->checkWord($_GET['word'])) {
			return;
		}

		$UserDictionary->write(array(
			'user_id' => $_GET['userId'],
			'word' => $_GET['word'],
		));
	}

	/**
	 * Write word in db
	 */
	public function delete() {
		if (empty($_GET['id'])) {
			return;
		}

		$UserDictionary = $this->_loadModel('UserDictionary');
		$UserDictionary->delete($_GET['id']);
	}

}