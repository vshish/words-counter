<?php

class PageController extends \Application\Controller\AController
{

	/**
	 * @var string - name Controller
	 */
	public $name = 'Page';

	/**
	 * Action for page About project Words Counter
	 */
	public function about()
	{
		$this->_render('about');
	}

}