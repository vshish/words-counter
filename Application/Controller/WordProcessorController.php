<?php

class WordProcessorController extends \Application\Controller\AController
{

	/**
	 * @var string - name Controller
	 */
	public $name = 'WordProcessor';

	/**
	 * Action for render form count words view
	 */
	public function index()
	{
		$this->_render();
	}

	/**
	 * Action for render user count words view
	 */
	public function userIndex()
	{
		$this->_render('user_index');
	}

	/**
	 * Action for processing and show result count words
	 */
	public function processing()
	{
		$WordProcessor = $this->_loadModel('WordProcessor');
		$countWords = $WordProcessor->countWords($_POST);

		$this->_set('countWords', $countWords);
		$this->_render('processing');
	}

	/**
	 * Action for processing and show result count users words
	 */
	public function userProcessing()
	{
		$userId = 1;
		$WordProcessor = $this->_loadModel('WordProcessor');
		$countWords = $WordProcessor->countUserWords($_POST, $userId);

		$this->_set('countWords', $countWords);
		$this->_set('userId', $userId);
		$this->_render('user_processing');
	}

}