<?php
namespace Application\Controller;

abstract class AController
{

	/**
	 * @var string - name Controller
	 */
	public $name;

	/**
	 * @var array - variables send to View
	 */
	protected $_viewVars = array();

	/**
	 * Render View for controller action
	 * 
	 * @param string $viewName - view name
	 */
	protected function _render($viewName = 'index')
	{
		$pth =  VIEW . DS . $this->name . DS . $viewName . '.html';

		$view = $this->_evaluate($pth, $this->_viewVars);
		$content = array('content' => $view);
		$page = $this->_evaluate(VIEW . DS . 'Layout' . DS . 'Template.html', $content);

		echo $page;
	}
	/**
	 * Set view variables
	 *
	 * @param string $name - var name
	 * @param mixed
	 */
	protected function _set($name, $content)
	{
		$this->_viewVars[$name] = $content;
	}

	/**
	 * Load and get Model by Name
	 *
	 * @param string
	 * @return object
	 */
	protected function _loadModel($modelName)
	{
		if (file_exists(MODEL . DS . $modelName. '.php')) {
			require MODEL . DS . $modelName. '.php';
			return new $modelName();
		}
	}

	protected function _evaluate($viewFile, $dataForView = array())
	{
		extract($dataForView);
		ob_start();

		include $viewFile;

		return ob_get_clean();
	}

}