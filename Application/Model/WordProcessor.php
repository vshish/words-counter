<?php

class WordProcessor extends \Application\Model\AModel
{

	private $__minCount = 2;

	private $__countExclusionWords = 300;

	private  $__symbolToRemove = array(
		'.', ',', '=', '<', '>', '/', '\\', '+', '-', '[', ']', '{', '}', '%',
		'!', '@', '#', '^', ':', ';', '"', '\'', '$', '(', ')', '?', '*', '&',
		'©', '1', '2', '3', '4', '5', '6', '7', '8', '9', '0',
	);

	public function countWords($data)
	{
		$this->__countExclusionWords = $data['count_exclusion_words'];
		$text = $this->_preparationText($data['text']);
		$words = $this->_getWords($text);

		$wordsCount = array_count_values($words);

		$this->_filterWord($wordsCount);

		arsort($wordsCount);

		return $wordsCount;
	}

	protected function _preparationText($text)
	{
		$text = str_replace($this->__symbolToRemove, ' ', $text);

		return (string)trim($text);
	}

	protected function _getWords($text)
	{
		$words = preg_split('/\s+/im', $text);

		array_walk($words, function(&$word) {
			$word = strtolower($word);
		});

		return $words;
	}

	protected function _filterWord(&$words)
	{
		$this->_exclusionKnownWords($words);
		$this->_deleteLessThenNeed($words);
	}

	protected function _exclusionKnownWords(&$words)
	{
		$excludeWords = $this->_getWordOnExect();

		foreach ($words as $word => $count) {
			if (in_array(strtolower($word), $excludeWords)) {
				unset($words[$word]);
			}
		}
	}

	protected function _getWordOnExect()
	{
		$str = file_get_contents(FILE_SOURCE . DS . 'users_dictionary.xml');
		$xml = simplexml_load_string($str);

		$exclusionWords = (array)$xml->body->word;
		$exclusionWords = array_slice($exclusionWords, 0, $this->__countExclusionWords);

		return (array)$exclusionWords; 
	}

	protected function _deleteLessThenNeed(&$words)
	{
		foreach ($words as $word => $count) {
			if ($count < $this->__minCount) {
				unset($words[$word]);
			}
		}
	}

	public function countUserWords($data, $userId)
	{
		$text = $this->_preparationText($data['text']);
		$words = $this->_getWords($text);

		$wordsCount = array_count_values($words);

		$this->_exclusionKnownUserWords($wordsCount, $userId);
		$this->_deleteLessThenNeed($words);

		arsort($wordsCount);

		return $wordsCount;
	}

	protected function _exclusionKnownUserWords(&$words, $userId = 0)
	{
		$userWords = $this->_getWordOnExectUser($userId);
		$excludeWords = array();
		foreach ($userWords as $word) {
			$excludeWords[] = $word['word'];
		}

		foreach ($words as $word => $count) {
			if (in_array(strtolower($word), $excludeWords)) {
				unset($words[$word]);
			}
		}
	}

	protected function _getWordOnExectUser($userId)
	{
		$modelName = $userId == 0 ? 'Dictionary' : 'UserDictionary';
		require MODEL . DS . $modelName . '.php';

		$Dictionary = new $modelName();
		$exclusionWords = $Dictionary->getWords($userId);

		return (array)$exclusionWords; 
	}
}