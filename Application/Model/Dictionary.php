<?php

class Dictionary extends \Application\Model\AModel
{
	protected $_useTable = 'dictionary';

	public function getWords()
	{
		$str = file_get_contents(FILE_SOURCE . DS . 'users_dictionary.xml');
		$xml = simplexml_load_string($str);

		return (array)$xml->body->word; 
	}
}