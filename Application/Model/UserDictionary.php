<?php

class UserDictionary extends \Application\Model\AModel
{

	protected $_useTable = 'user_dictionary';

	protected $_userId;

	public function getWords($userId)
	{
		return $this->_DataSource->findAllBy(array('user_id' => $userId));
	}

	public function write($word)
	{
		return $this->_DataSource->insert($word); 
	}

	public function delete($id)
	{
		return $this->_DataSource->delete($id); 
	}

	public function setUserId($userId)
	{
		$this->_userId = $userId;
	}

	public function checkWord($word)
	{
		return (boolean)$this->_DataSource->findOneBy(array(
			'user_id' => $this->_userId, 'word' => $word,
		));
	}

}