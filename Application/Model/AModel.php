<?php
namespace Application\Model;

abstract class AModel
{

	protected $_useTable = false;

	protected $_DataSource;

	public function __construct()
	{
		if ((bool)$this->_useTable) {
			$this->_DataSource = new \Library\DataBaseSource($this->_useTable);
		}
	}
}