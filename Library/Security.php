<?php
namespace Library;

/**
 * Security
 */
abstract class Security
{

	public static function xssFilter($string)
	{
		return strip_tags($string, '<p><a>');
	}

}