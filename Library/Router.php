<?php
namespace Library;

Class Router
{
	/**
	 * Load need class and call his action
	 * 
	 * @return voit
	 */
	public static function run()
	{
		$action = self::getControllerAndAction();

		try {
			require_once CONTROLLER . DS . $action['controller'] . '.php';
		} catch (Exception $Exc) {
			throw new \Exception('Page Not Found!');
		}

		$Controller = new $action['controller']();
		$Controller->{$action['action']}();
	}

	/**
	 * Return controller name and his action name
	 * 
	 * @return array
	 */
	public static function getControllerAndAction()
	{
		if (!preg_match('%path=(\w+)/(\w+)%i', $_SERVER['QUERY_STRING'])) {
			return array(
				'controller' => 'WordProcessorController',
				'action' => 'index'
			);
		}

		preg_match('%path=(\w+)/(\w+)%i', $_SERVER['QUERY_STRING'], $matches);
		list(, $controller, $action) = $matches;

		return array(
			'controller' => $controller . 'Controller',
			'action' => $action
		);
	}
}