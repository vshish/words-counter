<?php
namespace Library;

/**
 * Autoloader for automat loading classes
 */
class Autoloader
{

	/**
	 * Load class 
	 * 
	 * @param  string $file
	 * @return voit
	 */
	public static function autoload($file)
	{
		$file = str_replace('\\', '/', $file);
		$systemPath = str_replace(DS . 'Library', '', __DIR__);
		$filePath = $systemPath . DS .  $file . '.php';

		if (file_exists($filePath)) {
			require_once($filePath);
		} else {
			// TODO:don`t work!
			throw new \Exception('File' . $file . ' not found!');
		}
	}

}

\spl_autoload_register('Library\Autoloader::autoload');