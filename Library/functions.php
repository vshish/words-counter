<?php

/**
 * Get Html link
 * 
 * @param  string $text
 * @param  string $url
 * @param  array  $options
 * @return string
 */
function getLink($text, $url = '/', $options = array())
{
	$href = '/';

	if ($url != '/') {
		$href = '/?path=';
		$href .= !empty($url['controller']) ? $url['controller'] : 'Index';
		$href .= '/';
		$href .= !empty($url['action']) ? $url['action'] : 'index';

		if (!empty($url['&'])) {
			foreach ($url['&'] as $name => $value) {
				$href .= '&' . $name . '=' . $value;
			}
		}
	}

	return '<a href="' . $href . '">' . $text . '</a>';
}

/**
 * Get local url by options
 * 
 * @param  array  $options
 * @return string
 */
function getLocalUrl($options = array())
{
	return '';
}