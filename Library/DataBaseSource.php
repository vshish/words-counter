<?php
namespace Library;

class DataBaseSource
{

	/**
	 * Data bases settiogs for connect
	 * 
	 * @var array
	 */
	private $__settings;

	/**
	 * Name table
	 * 
	 * @var string
	 */
	private $__useTable;

	/**
	 * Databases connect resource
	 * 
	 * @var resource
	 */
	private $__DbConnect;

	const DEFAULT_OPERATOR = '=';

	public function __construct($table)
	{
		$this->__useTable = $table;
		$this->__settings = require CONFIG . DS . 'db_settings.php';
	}

	public function query($sql)
	{
		$this->__connect();
		$resource = mysqli_query($this->__DbConnect, "SELECT * FROM $this->__useTable");
		$result = mysqli_fetch_assoc($resource);
		$this->__close();

		return $result;
	}

	/**
	 * Create connection with data base
	 * 
	 * @return voit
	 */
	private function __connect()
	{
		$this->__DbConnect = mysqli_connect(
			$this->__settings['host'],
			$this->__settings['login'],
			$this->__settings['password'],
			$this->__settings['database']
		);
	}

	private function __close()
	{
		mysqli_close($this->__DbConnect);
	}

	public function getById($id)
	{
		$id = (integer)$id;

		$this->__connect();
		$resource = mysqli_query($this->__DbConnect, "SELECT * FROM $this->__useTable WHERE `id` = $id");
		$result = mysqli_fetch_assoc($resource);
		$this->__close();

		return $result;
	}

	/**
	 * Get all records satisfying the condition
	 * 
	 * @param  array $options
	 * @return array
	 */
	public function findAllBy($options = array())
	{
		$result = array();
		$sql = $this->_buildQuery($options);

		$this->__connect();
		$resource = mysqli_query($this->__DbConnect, $sql);

		for ($line = 1; $line <= mysqli_num_rows($resource); $line++) {
			$result[] = mysqli_fetch_assoc($resource);
		}

		$this->__close();

		return $result;
	}

	/**
	 * Return One first record satisfying the condition
	 * 
	 * @param  array $options
	 * @return array
	 */
	public function findOneBy($options = array())
	{
		$sql = $this->_buildQuery($options);

		$this->__connect();
		$resource = mysqli_query($this->__DbConnect, $sql);
		$result = mysqli_fetch_assoc($resource);
		$this->__close();

		return $result;
	}

	protected function _buildQuery($options)
	{
		$sql = "SELECT * FROM $this->__useTable ";
		$sql .= $this->_buildQueryWhere($options);

		return $sql;
	}

	protected function _buildQueryWhere($options)
	{
		$where = ' WHERE ';
		$condition = array();

		if (empty($options)) {
			return $where . '1 = 1';
		}

		foreach ($options as $fieldName => $fieldValue) {
			$queryValue = is_numeric($fieldValue) ? (integer)$fieldValue : "'$fieldValue'";

			if (!preg_match('/(<|>|=|!=)/', $fieldName)) {
				$fieldName .= self::DEFAULT_OPERATOR;
			}

			$condition[] = " ($fieldName $queryValue) ";
		}

		return $where . implode(' AND ', $condition);
	}

	/**
	 * Insert new records in databases
	 * 
	 * @param  array $data
	 * @return voit
	 */
	public function insert($data)
	{
		$fields = array();
		$values = array();

		foreach ($data as $fieldName => $fieldValue) {
			$fields[] = "`$fieldName`";
			$values[] = "'$fieldValue'";
		}
		$fields = implode(', ', $fields);
		$values = implode(', ', $values);

		$this->__connect();

		$sql = "INSERT INTO $this->__useTable ($fields) VALUES ($values)";
		mysqli_query($this->__DbConnect, $sql);
		$this->__close();
	}

	public function delete($id)
	{
		$this->__connect();

		$sql = "DELETE FROM $this->__useTable WHERE id = '$id'";
		mysqli_query($this->__DbConnect, $sql);
		$this->__close();
	}

}